package demo;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.aop.ClearInterceptor;
import com.jfinal.aop.ClearLayer;
import com.jfinal.core.Controller;

import demo.model.MyFile;
import demo.util.FileUtil;

@ClearInterceptor(ClearLayer.ALL)
public class HelloController extends Controller {
	

	public void index() {
		List<MyFile> fileList = new ArrayList<MyFile>();
		try{
		fileList = FileUtil.readFile(DemoConfig.p.getProperty("upload_path"));
		setAttr("fileList", fileList);
		render("file_list.html");
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
