package demo;

import java.io.File;

import com.jfinal.aop.ClearInterceptor;
import com.jfinal.aop.ClearLayer;
import com.jfinal.core.Controller;

public class DownController extends Controller {
	@ClearInterceptor(ClearLayer.ALL)
	public void index() {
		try {
			String filePath = DemoConfig.p.getProperty("upload_path") + "/"
					+ getPara("fname");
			File file = new File(filePath);
			renderFile(file);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return;
	}

	public void del() {
		try {
			String filePath = DemoConfig.p.getProperty("upload_path") + "/"
					+ getPara("fname");
			File file = new File(filePath);
			if (file.exists()) {
				file.delete();
			}

		} catch (Exception e) {
			setAttr("msg", "ɾ��ʧ��!!!");
			e.printStackTrace();
		}
		setAttr("msg", "ɾ���ɹ�!!!");
		forwardAction("/upload");
	}
}
