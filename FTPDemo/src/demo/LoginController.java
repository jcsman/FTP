package demo;

import com.jfinal.aop.ClearInterceptor;
import com.jfinal.aop.ClearLayer;
import com.jfinal.core.Controller;

@ClearInterceptor(ClearLayer.ALL)
public class LoginController extends Controller {

	public void index() {
		render("login.html");
	}

	public void check() {
		try {
			String uname = getPara("uname");
			String pwd = getPara("pwd");
			if (DemoConfig.p.getProperty("user").equals(uname)
					&& DemoConfig.p.getProperty("password").equals(pwd)) {
				getSession().setAttribute("uname", uname);
				getSession().setAttribute("pwd", pwd);
				this.forwardAction("/upload");
			} else {
				setAttr("msg", "�û������������");
				render("login.html");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
