package demo.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;
import com.jfinal.core.Controller;

public class AuthInterceptor implements Interceptor {
	public void intercept(ActionInvocation ai) {
		Controller controller = ai.getController();
		String uname = controller.getSessionAttr("uname");
		String pwd = controller.getSessionAttr("pwd");
		if (uname != null && pwd != null) {
			ai.invoke();
		} else {
			controller.forwardAction("/login");
		}

	}
}
