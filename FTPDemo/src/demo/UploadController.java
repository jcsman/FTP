package demo;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.core.Controller;

import demo.model.MyFile;
import demo.util.FileUtil;

public class UploadController extends Controller {

	public void index() {
		List<MyFile> fileList = new ArrayList<MyFile>();
		fileList = FileUtil.readFile(DemoConfig.p.getProperty("upload_path"));
		setAttr("fileList", fileList);
		render("upload.html");
	}

	public void save() {
		createToken("blogToken", 30 * 60); // 过期时间设置为30分钟
		String msg = "";
		String uploadPath = DemoConfig.p.getProperty("upload_path");
		try {
			if (getFile("myfile") != null) {
				// 注释
				getFile("myfile", uploadPath, 104857600, "UTF-8");
				msg = "上传成功!!!";
			} else {
				msg = "请选择文件!!!";
			}
		} catch (Exception e) {
			msg = "上传失败!错误信息!!!" + e.toString();
			e.printStackTrace();
		}
		setAttr("msg", msg);
		index();
	}
}
