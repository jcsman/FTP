package demo;

import java.util.Properties;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.render.ViewType;

import demo.interceptor.AuthInterceptor;

public class DemoConfig extends JFinalConfig {
	public static Properties p;

	@Override
	public void configConstant(Constants arg0) {
		p = loadPropertyFile("conf.properties");
		arg0.setDevMode(getPropertyToBoolean("devMode", true));
		arg0.setViewType(ViewType.FREE_MARKER);
		arg0.setUploadedFileSaveDirectory(p.getProperty("upload_path"));
	}

	@Override
	public void configHandler(Handlers arg0) {

	}

	@Override
	public void configInterceptor(Interceptors arg0) {
		arg0.add(new AuthInterceptor());
	}

	@Override
	public void configPlugin(Plugins arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void configRoute(Routes me) {
		me.add("/", HelloController.class, "/hello");// 默认文件列表
		me.add("/login", LoginController.class, "login"); // 管理员登录窗口
		me.add("/upload", UploadController.class); // 文件上传控制
		me.add("/down", DownController.class); // 文件下载控制
	}

}
